package com.example.noteapp.model

enum class NoteFormActions {
    EDIT, ADD
}