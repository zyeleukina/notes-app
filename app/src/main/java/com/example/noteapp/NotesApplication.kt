package com.example.noteapp

import android.app.Application
import com.example.noteapp.db.NoteDatabase

class NotesApplication : Application() {
    val database by lazy { NoteDatabase.getDatabase(this) }
    val repository by lazy { NotesRepository(database.noteDao()) }
}