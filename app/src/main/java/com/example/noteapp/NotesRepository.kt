package com.example.noteapp

import androidx.annotation.WorkerThread
import com.example.noteapp.db.NoteDao
import com.example.noteapp.model.Note
import kotlinx.coroutines.flow.Flow
import java.time.LocalDateTime

class NotesRepository(private val database: NoteDao) {
    val allNotes: Flow<List<Note>> = database.getAllNotes()

    @WorkerThread
    suspend fun addNewNote(note: Note) {
        database.addNote(note)
    }

    fun getNoteDetails(id: Int): Flow<Note> = database.getNoteDetails(id)

    suspend fun deleteNote(note: Note) {
        database.deleteNote(note)
    }

    suspend fun updateNote(note: Note) {
        database.updateNote(note.copy(lastUpdateTime = LocalDateTime.now().toString()))
    }
}